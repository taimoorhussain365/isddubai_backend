<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Subscription extends Model
{

    protected $appends = ['academyName','total'];
    protected $with =['comment','payments'];
    public $additional_attributes = ['full_name'];

    public function getAcademyNameAttribute($v)
    {
        return Academy::where('id',$this->academy)->value('title');
    }
    
    public function academies()
    {
        return $this->belongsTo('App\Academy', 'academy');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'subscriber');
    }

    public function parentName()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    
    public function comment(){
        return $this->hasMany('App\Comment', 'subscriber_id','subscriber');
    } 

    public function payments(){
        return $this->hasMany('App\Payment','book_id','id')->where('status',1);   
    }     

    public function termss()
    {
        return $this->belongsToMany(Term::class, 'povite_subscriptions_terms', 'term_id', 'term_id');
    }

    public function classes()
    {
        return $this->belongsToMany(Classes::class, 'povite_subscriptions_classes');
    }
    
    public function getTotalAttribute($v)
    {
        return Payment::where('book_id',$this->id)->where('rent',2)->sum('amount');
    }     

    public function getSubscriberAttribute($v)
    {
        return  $v != null ?  User::where('id',$this->attributes['subscriber'])->value('name') : '';
    } 

    public function getAgeAttribute($v)
    {
        return  $v != null ?  Age::where('id',$this->attributes['age'])->value('title') : '';
    } 


    public function getFullNameAttribute()
    {
        $link = '<a target="_blank" href="http://bookings.isddubai.com/admin/subscriptions/'.$this->attributes['id'].'">'.$this->attributes['id'].'<a/>';
        echo  $link;
    }
    
    public function packages(){
        return $this->belongsTo('App\Package', 'package','id');
    } 
    
    public function getTermsAttribute($v)
    {

        // dd($v);

        if($v != null){
            $newVal = json_decode($v); 
            $dataSet = array();
            foreach ($newVal as $item) {
             $name = Term::where('id',$item)->value('name');
             $dataSet[] = $name;
            }
            return  implode(",",$dataSet);
        }
    }

}
