<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Academy;

class AlertClassAttention extends Mailable
{
    use Queueable, SerializesModels;
    public $subscription;
    public $academy;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subscription,$academy)
    {
        //
        $this->subscription  = $subscription;
        $this->academy  = $academy;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $subscription = $this->subscription ;
        $academy = $this->academy ;
        $academyName = Academy::find($academy)->value('title'); 
        return $this->subject($academyName .' inquiry')->view('emails.alert_inquiry');
    }
}
