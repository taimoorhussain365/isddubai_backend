<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CourtTime;
use App\Court;
use DateTime;
use App\Court_books;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Events\BreadDataAdded;
use App\Court_times;
use Carbon;
use App\Booking;
use TCG\Voyager\Events\BreadDataUpdated;

class CourtsController  extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{

    public function store(Request $request)
    {
        if($request->parent > 0){
            $allSize = Court::where('parent',$request->parent)->sum('size');
            if($allSize >= 100){
                $data =
                [
                    'message'    => "you can't add more courts over 100%",
                    'alert-type' => 'error',
                ];
            return redirect()->back()->with($data);              
            }
        }       
        $slug = $this->getSlug($request);
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        $this->authorize('add', app($dataType->model_name));
        $val = $this->validateBread($request->all(), $dataType->addRows)->validate();
        $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());
        event(new BreadDataAdded($dataType, $data));  
            foreach($request->day as $row => $name){
                $day = $name;
                $from = $_POST['from'][$row];
                $to = $_POST['to'][$row];
                $court_times = new Court_times();
                $court_times->day = $day;
                $court_times->from = $from;
                $court_times->to = $to;
                $court_times->court_id = $data->id;
                $court_times->save();
            }  
            Return redirect($request->only('redirects_to'));


    }

    public function update(Request $request ,$id)
    {
        //dd($request->all());
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Compatibility with Model binding.
        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;

        $model = app($dataType->model_name);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        if ($model && in_array(SoftDeletes::class, class_uses_recursive($model))) {
            $data = $model->withTrashed()->findOrFail($id);
        } else {
            $data = $model->findOrFail($id);
        }

        // Check permission
        $this->authorize('edit', $data);

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id)->validate();
        $this->insertUpdateData($request, $slug, $dataType->editRows, $data);

        event(new BreadDataUpdated($dataType, $data));
        Court_times::where('court_id',$id)->delete();
        foreach($request->day as $row => $name){
            $day = $name;
            $from = $_POST['from'][$row];
            $to = $_POST['to'][$row];
            $court_times = new Court_times();
            $court_times->day = $day;
            $court_times->from = $from;
            $court_times->to = $to;
            $court_times->court_id = $data->id;
            $court_times->save();
        } 

        if (auth()->user()->can('browse', app($dataType->model_name))) {
            $redirect = redirect()->route("voyager.{$dataType->slug}.index");
        } else {
            $redirect = redirect()->back();
        }

        return $redirect->with([
            'message'    => __('voyager::generic.successfully_updated')." {$dataType->getTranslatedAttribute('display_name_singular')}",
            'alert-type' => 'success',
        ]);        
    }

    public function CheckTime(Request $request ,$id)
    {    
        $check = CourtTime::where('day',$id)->with('court')->get();
        return response()->json([
            'state'=>true,
            'msg' => 200,
            'data'=>$check
        ]);
    }

    public function CheckSport(Request $request)
    {   
        $duration = $request->duration;
        $to = \Carbon\Carbon::parse($request->from)->addMinutes($duration)->format('H:i');
        if($request->sport == 5){
            $check = Court::where('sports',$request->sport)->get();
        }else{
            $check1 = CourtTime::where('day',$request->day)
            ->whereTime('from','<=',\Carbon\Carbon::parse($request->from))
            ->whereTime('to','>=',\Carbon\Carbon::parse($to))
            ->pluck('court_id');
            $check = Court::where('sports',$request->sport)->whereIn('id', $check1)->get();
        }

        return response()->json([
            'state'=>200,
            'msg' => true,
            'data'=>$check
        ]);
    }

}