<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Hash;
use Validator;
use Auth;
use DB;
use Mail;
use App\Court;
use App\Sport;
use App\Booking;
use App\CourtTime;
use App\Product;
use App\Academy;

class CourtsController extends Controller
{

    public function list(Request $request)
    {
        $data = Court::where('active',1)->orderBy('id', 'desc')->get();
        return response()->json([
            'status'=>true,
            'msg' => 200,
            'data'=>$data
        ]);
    }


    public function Products($id){
        $data = Product::whereHas('sport', function($q) use ($id){
            $q->where('sport_id', $id);
        })->where('private', 1)->get();
        return response()->json([
            'state'=>true,
            'msg' => 200,
            'data'=>$data
        ]);
    }

    public function Show(Request $request,$id)
    {
        $data = Court::where('id',$id)->with(array('sport' => function($query) {
            $query->select('sport_id');
        }))->first();
        return response()->json([
            'status'=>true,
            'msg' => 200,
            'data'=>$data
        ]);
    }

    public function Sport(){
        $data = Sport::all();
        $name = Academy::select('title')->get();
        return response()->json([
            'status'=>true,
            'msg' => 200,
            'data'=>$data,
            'name'=>$name,
        ]);
    }

    public function SportName($id){
        $data = Sport::where('id',$id)->first();
        $name = Academy::select('title')->get();
        return response()->json([
            'status'=>true,
            'msg' => 200,
            'data'=>$data,
            'name'=>$name,
        ]);
    }

    public function SportDetails($id){
        $data = Court::where('sports',$id)->where('show',1)->get();
        return response()->json([
            'status'=>true,
            'msg' => 200,
            'data'=>$data
        ]);
    }

    public function CheckAvailability(Request $request){
        $check11 = Booking::where('date',$request->date)
        ->whereTime('from','<=',\Carbon\Carbon::parse($request->time))
        ->whereTime('to','>=',\Carbon\Carbon::parse($request->time))
        ->where('pitch',$request->item_id)
        ->count();
        
        if($check11 > 0){
            return response()->json([
                'status'=>true,
                'msg' => 200,
            ]);
        }else{
            return response()->json([
                'status'=>false,
                'msg' => 500,
            ]);
        }
    }

    public function CheckTime(Request $request)
    {
        if($request->id != 5){
            $time = $request->time;
            $duration = $request->duration;
            $to = \Carbon\Carbon::parse($request->time)->addMinutes($duration)->format('H:i');
            $day = date('l', strtotime($request->date));
            $data = Court::where('sports',$request->id)->whereHas('Times', function($query) use($day,$time,$to) {
                $query->where('day', '=', $day)->whereTime('from','<=',\Carbon\Carbon::parse($time))->whereTime('to','>=',\Carbon\Carbon::parse($to));
            })->whereNotIn('id', function ($query) use($request)  {
                $query->select('pitch')->from('bookings')->where('date',$request->date)->where('from',$request->time);
            })->get();

        }else{
            $data = Court::where('sports',$request->id)->get();
        }
        return response()->json([
            'data'=> $data ,
        ]);
    }
}