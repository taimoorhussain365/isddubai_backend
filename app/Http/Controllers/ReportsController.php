<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Court_times;
use App\Booking;
use Carbon;
use DB;
use App\Exports\BookingExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Subscription;
use App\Academy;
use App\Payment;

class ReportsController  extends Controller
{

    public function Booking()
    {
      $data = Booking::whereDate('created_at', Carbon\Carbon::today())->with('user','products')->get();
      return view('reports.booking')->with('data',$data);
    }

    public function Schedule()
    {
      $q =  Booking::with('user','products'); 
      if(isset($_GET['court']) && $_GET['court'] > 0){
        $q->where('pitch', $_GET['court']);
      }
      if(isset($_GET['users']) && $_GET['users'] > 0){
          $q->where('client_id', $_GET['users']);
      }
      if(isset($_GET['from']) && $_GET['from'] > 0 && isset($_GET['to']) && $_GET['to'] > 0){
          $q->whereBetween('created_at', [date($_GET['from']), date($_GET['to'])]);
      }        
      $data = $q->get();
      $sum = $q->sum('total');
      $sum2 = 0;
      foreach ($data as $key) {
        $v =  $key->vat * $key->total / 100;
        $sum2 += $key->total + $v;
      }
      return view('reports.schedule')->with('data',$data)->with('sum',$sum)->with('sum2',$sum2);
    }

    public function Finance()
    {
      $q =  Booking::with('user','products'); 
      if(isset($_GET['court']) && $_GET['court'] > 0){
        $q->where('pitch', $_GET['court']);
      }
      if(isset($_GET['users']) && $_GET['users'] > 0){
          $q->where('client_id', $_GET['users']);
      }
      if(isset($_GET['from']) && $_GET['from'] > 0 && isset($_GET['to']) && $_GET['to'] > 0){
          $q->whereBetween('created_at', [date($_GET['from']), date($_GET['to'])]);
      }        
      $data = $q->get();
      $sum = $q->sum('total');
      $sum2 = 0;
      foreach ($data as $key) {
        $v =  $key->vat * $key->total / 100;
        $sum2 += $key->total + $v;
      }
      return view('reports.finance')->with('data',$data)->with('sum',$sum)->with('sum2',$sum2);
    }    


    public function Academy()
    {
    //   $q =  Payment::where('rent',2)->with('Subscription');
      // $q =  Payment::select('payment.*','pivote_academy_payment.refund_amnt','pivote_academy_payment.refund_resn','pivote_academy_payment.waveoff_amnt','pivote_academy_payment.waveoff_resn')->leftJoin('pivote_academy_payment', 'payment.book_id','pivote_academy_payment.book_id')->where('payment.rent',2);
      $q =  Payment::select(DB::raw('payment.* , SUM(pivote_academy_payment.refund_amnt) as refund_amnt, pivote_academy_payment.refund_resn , pivote_academy_payment.waveoff_amnt, pivote_academy_payment.waveoff_resn , users.name'))
                    ->leftJoin('pivote_academy_payment',  'pivote_academy_payment.book_id','payment.book_id')
                    ->leftJoin('users',  'users.id', 'payment.client_id')
                    ->where('payment.rent', 2)
                    ->where('payment.status', 1)
                    ->groupBy('pivote_academy_payment.book_id');

      if(isset($_GET['sport']) && $_GET['sport'] > 0){
        $sport =  $_GET['sport'];
        $q->whereHas('academies', function ($q) use ($sport) {
            $q->where('sport', $sport);
        });        
      }       

      if(isset($_GET['age']) && $_GET['age'] > 0){
        $age =  $_GET['age'];
        $q->whereHas('academies', function ($q) use ($age) {
            $q->where('age', $age);
        });        
      } 

      if(isset($_GET['academies']) && $_GET['academies'] > 0){
        $academies =  $_GET['academies'];
        $q->whereHas('academies', function ($q) use ($academies) {
            $q->where('academies', $academies);
        });        
      } 

      if(isset($_GET['from']) && $_GET['from'] > 0 && isset($_GET['to']) && $_GET['to'] > 0){
          $q->whereBetween('created_at', [date($_GET['from']), date($_GET['to'])]);
      } 
      
      $data = $q->get();
      $sum = 0;
      $sum2 = 0;
      
      // dd($data);
      
      return view('reports.academy')->with('data',$data)->with('sum',$sum)->with('sum2',$sum2);
    }

    public function getRefundReason(Request $request)
    {
        $pivote_academy_payment = DB::table('pivote_academy_payment')->where('client_id', $request->parent_id)->where('book_id', $request->academy_id)->get();

        // dd($pivote_academy_payment);

 

        if (!$pivote_academy_payment->isEmpty()) {
            foreach ($pivote_academy_payment as $key => $data) {

                $d[] = [
                    'status' => 200,
                    'amnt' =>$data->refund_amnt,
                    'msg' => $data->refund_resn,
                ];

               
            }
        } else {

            $d[] = [
                'status' => 500,
                'amnt' =>0,
                'msg' => 'History not found!'
            ];
        }

        // print_r($d);

        return json_encode($d);
    }
}