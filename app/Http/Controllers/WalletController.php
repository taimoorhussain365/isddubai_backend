<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CourtTime;
use App\Court;
use DateTime;
use App\Court_books;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Events\BreadDataAdded;
use App\Court_times;
use App\Booking;
use App\OrderProduct;
use App\Product;
use Carbon;
use Illuminate\Support\Facades\Auth;
use App;
use PDF;
use App\Payment;
use App\pivote_academy_payment;
use DB;
use Mail;
use App\User;
use App\Subscription;
use App\Wallet;
use Carbon\Carbon as CarbonCarbon;
use TCG\Voyager\Database\Schema\SchemaManager;

class WalletController  extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{

    // public function index(Request $request)
    // {

    //     $data = Wallet::where('client_id', $request->data['id'])->get();


    //     // dd($data);

    //     return view('voyager::client-wallets.browse');
    // }

    public function index(Request $request)
    {

        // GET THE CLIENT ID
        $clientId = $request->data['id'];

        // get last total amount of wallet                
        $total = Wallet::where('client_id', $clientId)->orderBy('id', 'DESC')->first();


        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', 'wallets')->first();

        // Check permission
        // $this->authorize('browse', app($dataType->model_name));

        $getter = $dataType->server_side ? 'paginate' : 'get';

        $search = (object) ['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];

        $searchNames = [];
        if ($dataType->server_side) {
            $searchable = SchemaManager::describeTable(app($dataType->model_name)->getTable())->pluck('name')->toArray();
            $dataRow = Voyager::model('DataRow')->whereDataTypeId($dataType->id)->get();
            foreach ($searchable as $key => $value) {
                $field = $dataRow->where('field', $value)->first();
                $displayName = ucwords(str_replace('_', ' ', $value));
                if ($field !== null) {
                    $displayName = $field->getTranslatedAttribute('display_name');
                }
                $searchNames[$value] = $displayName;
            }
        }

        $orderBy = $request->get('order_by', $dataType->order_column);
        $sortOrder = $request->get('sort_order', $dataType->order_direction);
        $usesSoftDeletes = false;
        $showSoftDeleted = false;

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope' . ucfirst($dataType->scope))) {
                $query = $model->{$dataType->scope}();
            } else {
                $query = $model::select('id', 'debit_amount', 'credit_amount', 'remarks', 'total', 'created_at')->where('client_id', $clientId);
            }

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses_recursive($model)) && Auth::user()->can('delete', app($dataType->model_name))) {
                $usesSoftDeletes = true;

                if ($request->get('showSoftDeleted')) {
                    $showSoftDeleted = true;
                    $query = $query->withTrashed();
                }
            }

            // If a column has a relationship associated with it, we do not want to show that field
            $this->removeRelationshipField($dataType, 'browse');

            if ($search->value != '' && $search->key && $search->filter) {
                $search_filter = ($search->filter == 'equals') ? '=' : 'LIKE';
                $search_value = ($search->filter == 'equals') ? $search->value : '%' . $search->value . '%';
                $query->where($search->key, $search_filter, $search_value);
            }

            if ($orderBy && in_array($orderBy, $dataType->fields())) {
                $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'desc';
                $dataTypeContent = call_user_func([
                    $query->orderBy($orderBy, $querySortOrder),
                    $getter,
                ]);
            } elseif ($model->timestamps) {
                $dataTypeContent = call_user_func([$query->latest($model::CREATED_AT), $getter]);
            } else {
                $dataTypeContent = call_user_func([$query->orderBy($model->getKeyName(), 'DESC'), $getter]);
            }

            // Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
        } else {
            // If Model doesn't exist, get data from table name
            $dataTypeContent = call_user_func([DB::table($dataType->name), $getter]);
            $model = false;
        }

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($model);

        // Eagerload Relations
        $this->eagerLoadRelations($dataTypeContent, $dataType, 'browse', $isModelTranslatable);

        // Check if server side pagination is enabled
        $isServerSide = isset($dataType->server_side) && $dataType->server_side;

        // Check if a default search key is set
        $defaultSearchKey = $dataType->default_search_key ?? null;

        // Actions
        $actions = [];
        if (!empty($dataTypeContent->first())) {
            foreach (Voyager::actions() as $action) {
                $action = new $action($dataType, $dataTypeContent->first());

                if ($action->shouldActionDisplayOnDataType()) {
                    $actions[] = $action;
                }
            }
        }

        // Define showCheckboxColumn
        $showCheckboxColumn = false;
        if (Auth::user()->can('delete', app($dataType->model_name))) {
            $showCheckboxColumn = true;
        } else {
            foreach ($actions as $action) {
                if (method_exists($action, 'massAction')) {
                    $showCheckboxColumn = true;
                }
            }
        }

        // Define orderColumn
        $orderColumn = [];
        if ($orderBy) {
            $index = $dataType->browseRows->where('field', $orderBy)->keys()->first() + ($showCheckboxColumn ? 1 : 0);
            $orderColumn = [[$index, $sortOrder ?? 'desc']];
        }

        $view = 'voyager::client-wallets.browse';

        // if (view()->exists("voyager::$slug.browse")) {
        //     $view = "voyager::$slug.browse";
        // }    

        // dd();

        return Voyager::view($view, compact(
            'clientId',
            'total',
            'actions',
            'dataType',
            'dataTypeContent',
            'isModelTranslatable',
            'search',
            'orderBy',
            'orderColumn',
            'sortOrder',
            'searchNames',
            'isServerSide',
            'defaultSearchKey',
            'usesSoftDeletes',
            'showSoftDeleted',
            'showCheckboxColumn'
        ));
    }

    public function create(Request $request)
    {
        $clientId = $request->client_id;

        $slug = $this->getSlug($request);

        // dd($slug);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        $dataTypeContent = (strlen($dataType->model_name) != 0)
            ? new $dataType->model_name()
            : false;

        foreach ($dataType->addRows as $key => $row) {
            $dataType->addRows[$key]['col_width'] = $row->details->width ?? 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'add');

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        // Eagerload Relations
        $this->eagerLoadRelations($dataTypeContent, $dataType, 'add', $isModelTranslatable);

        $view = 'voyager::client-wallets.edit-add';

        if (view()->exists("voyager::client-wallets.edit-add")) {
            $view = "voyager::client-wallets.edit-add";
        }

        // dd($dataType);

        return Voyager::view($view, compact('clientId', 'dataType', 'dataTypeContent', 'isModelTranslatable'));
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // dd($dataType);

        // Check permission
        // $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        // $val = $this->validateBread($request->all(), $dataType->addRows)->validate();

        // dd($request->client_id);




        // validation
        $request->validate([

            'amount_type' => 'required',
            'amount' => 'required|numeric',
            'remarks' => 'required',

        ], [

            'amount_type.required' => 'The Amount Type field is required',
            'amount.required' => 'The Amount must be a number',
            'remarks.required' => 'The Remark field is required',

        ]);



        // get last total amount of wallet                
        $last_wllt_amnt = Wallet::where('client_id', $request->client_id)->orderBy('id', 'DESC')->first();

        $amnt = 0;
        if ($last_wllt_amnt) {
            // dd('as dasd');
            $amnt = $last_wllt_amnt->total;
        } else {
            // dd('as assssss');
            // dd('else');
            $amnt = $last_wllt_amnt = 0;
        }
        // dd($final_amnt);

        // debit
        if ($request->amount_type == 1) {

            $final_amnt = $amnt - $request->amount;

            if ($amnt <= $request->amount) {

                return redirect()->back()->with([
                    'message'    => "Wallet balance is low!",
                    'alert-type' => 'info',
                ]);
            }

            $wallet = new Wallet();
            $wallet->client_id = $request->client_id;
            $wallet->debit_amount = $request->amount;
            $wallet->remarks = $request->remarks;
            $wallet->total = $final_amnt;
            $wallet->is_active = STATUS_ACTIVE;
            $wallet->created_at = CarbonCarbon::now()->toDateTimeString();
            $wallet->save();
        }
        // credit
        else if ($request->amount_type == 2) {
            $final_amnt = $amnt + $request->amount;

            // if($last_wllt_amnt->total >= $request->amount){

            //     return redirect()->back()->with([
            //         'message'    => "Wallet balance is low!",
            //         'alert-type' => 'info',
            //     ]);

            // }

            $wallet = new Wallet();
            $wallet->client_id = $request->client_id;
            $wallet->credit_amount = $request->amount;
            $wallet->remarks = $request->remarks;
            $wallet->total = $final_amnt;
            $wallet->is_active = STATUS_ACTIVE;
            $wallet->created_at = CarbonCarbon::now()->toDateTimeString();
            $wallet->save();
        }

        $data =
            [
                'id' => $request->client_id,
            ];


        return redirect()->route('voyager.wallets.index', compact('data'))->with([
            'message'    => __('voyager::generic.successfully_added_new') . "  {$dataType->getTranslatedAttribute('display_name_singular')}",
            'alert-type' => 'success',
        ]);

        // $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());

        // event(new BreadDataAdded($dataType, $data));

        // if (!$request->has('_tagging')) {
        //     if (auth()->user()->can('browse', $data)) {
        //         $redirect = redirect()->route("voyager.{$dataType->slug}.index");
        //     } else {
        //         $redirect = redirect()->back();
        //     }

        //     return $redirect->with([
        //         'message'    => __('voyager::generic.successfully_added_new')." {$dataType->getTranslatedAttribute('display_name_singular')}",
        //         'alert-type' => 'success',
        //     ]);
        // } else {
        //     return response()->json(['success' => true, 'data' => $data]);
        // }
    }
}
