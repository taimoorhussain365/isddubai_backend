<?php

namespace App\Actions;

use TCG\Voyager\Actions\AbstractAction;

class RefundAction extends AbstractAction
{
    public function getTitle()
    {
        return 'Refund / Waveoff'; 
    }

    public function getIcon()
    {
        return 'voyager-dollar';
    }

    // public function getPolicy()
    // {
    //     return 'read';
    // }

    public function getAttributes()
    {
       if($this->data->{'status'} == 1)
        {
            return [
                'class' => 'btn btn-sm btn-primary pull-center',
            ];
        }
        else
        {
            return [
                'class' => 'btn btn-sm btn-primary pull-left hide',
            ];
        }

    }

    public function shouldActionDisplayOnDataType()
    {
        return $this->dataType->slug == 'academy_payment' && $this->data->{'status'} == 1;
    }

    public function getDefaultRoute()
    {
        $data = $this->data->toArray();
        // dd($this->data->toArray());

        // $arr = [];
        // $arr = [
        //     'id'
        // ]
        return route('refund.index', compact('data'));
    }
}
