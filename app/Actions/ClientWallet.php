<?php

namespace App\Actions;

use TCG\Voyager\Actions\AbstractAction;

class ClientWallet extends AbstractAction
{
    public function getTitle()
    {
        return 'Wallet';
    }

    public function getIcon()
    {
        return 'voyager-dollar';
    }

    // public function getPolicy()
    // {
    //     return 'read';
    // }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-primary pull-left',
        ];
    }

    public function shouldActionDisplayOnDataType()
    {
        return $this->dataType->slug == 'clients';
    }

    public function getDefaultRoute()
    {
        $data = $this->data->toArray();
        // dd($this->data->toArray());

        // $arr = [];
        // $arr = [
        //     'id'
        // ]
        return route('voyager.wallets.index', compact('data'));
    }
}
