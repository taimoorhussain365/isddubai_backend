<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class pivote_academy_payment extends Model
{
    use SoftDeletes;
    protected $table = "pivote_academy_payment";

    protected $fillable = ['book_id','refund_amnt','refund_resn','waveoff_amnt','waveoff_resn','type', 'client_id'];
}
