<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Wallet extends Model
{

    use SoftDeletes;

    protected $table = 'wallets';

    protected $fillable = ['client_id','debit_amount','credit_amount','remarks','status','total','is_active'];

    
}
