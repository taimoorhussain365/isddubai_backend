@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop


@section('page_header')
    <h1 class="page-title"> <i class="voyager-bar-chart"></i> Academy Report</h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')

    <div class="select">
        <form action="" method="get">
            <div class="col-sm-12">

                <div class="col-sm-4">
                    <label for="staticEmail" class="col-sm-2 col-form-label">Sport</label>
                    <div class="col-sm-10">
                        <?php $sports = DB::table('sports')->get(); ?>
                        <select class="form-control input-sm" name="sport">
                            <option value="">Select Sport</option>
                            @foreach ($sports as $task)
                                <option value="{{ $task->id }}" @if (isset($_GET['sport']) && $_GET['sport'] > 0)
                                    @if ($task->id == $_GET['sport']) selected @endif
                            @endif>{{ $task->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <label for="staticEmail" class="col-sm-2 col-form-label">Age</label>
                    <div class="col-sm-10">
                        <?php $ages = DB::table('ages')->get(); ?>
                        <select class="form-control input-sm" name="age">
                            <option value="">Select Age</option>
                            @foreach ($ages as $task)
                                <option value="{{ $task->id }}" @if (isset($_GET['age']) && $_GET['age'] > 0)
                                    @if ($task->id == $_GET['age']) selected @endif
                            @endif>{{ $task->title }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>



                <div class="col-sm-4">
                    <label for="staticEmail" class="col-sm-2 col-form-label">Academy</label>
                    <div class="col-sm-10">
                        <?php $users = DB::table('academies')->get(); ?>
                        <select class="form-control input-sm" name="academies">
                            <option value="">Select Academy</option>
                            @foreach ($users as $task)
                                <option value="{{ $task->id }}" @if (isset($_GET['academies']) && $_GET['academies'] > 0)  @if ($task->id == $_GET['academies']) selected @endif @endif>{{ $task->title }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <label for="staticEmail" class="col-sm-2 col-form-label">From</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control" name="from" @if (isset($_GET['from']) && $_GET['from'] > 0) value="{{ $_GET['from'] }}" @endif>
                    </div>
                </div>
            
                <div class="col-sm-4">
                    <label for="staticEmail" class="col-sm-2 col-form-label">to</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control" name="to" @if (isset($_GET['to']) && $_GET['to'] > 0) value="{{ $_GET['to'] }}" @endif>
                    </div>
                </div>
                
                <button type="submit" class="btn btn-primary">Submit</button>

            </div>

        </form>
    </div>


    <div id="dvData">
        <table id="dataTable" style="width:100%;">
            <thead>
                <tr>
                    <th>Serial Number</th>
                    <th>Invoice Number</th>
                    <th>Parent name</th>
                    <th>Parent email</th>
                    <th>Subscriber Name</th>
                    <th>Transaction Code</th>
                    <th>Total Amount </th>
                    <th>VAT amount </th>
                    <th>Total Amount Excl VAT</th>
                    <th>Refund Amount</th>
                    <th>Refund Reason</th>
                    <th>Waveoff Amount</th>
                    <th>Waveoff Reason</th>
                    <th>Total Revenue</th>
                    
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $index => $item)
                <?php //dd($data);
                if($item->subscription){
                $parentName = App\User::where('id',$item->subscription->user_id)->value('name');
                $parentEmail = App\User::where('id',$item->subscription->user_id)->value('email');
                $vat =  $vat = 0.05 * $item->amount; }
                ?>
                    <tr>
                        <td>{{ $index +1 }}</td>
                        <td>#{{ $item->id }}</td>
                        <td>{{ $parentName}}</td>
                        <td>{{ $parentEmail}}</td>
                        <td>@if($item->subscription){{ $item->subscription->subscriber}} @endif</td>
                        <td>{{ $item->ref }}</td>
                        <td>{{ $item->amount }}</td>
                        <td>{{ $vat }}</td>
                        <td>{{ $item->amount - $vat}}</td>
                        <td>{{ $item->refund_amnt}}</td>
                        <td>{{ $item->refund_resn}}</td>
                        <td>{{ $item->waveoff_amnt}}</td>
                        <td>{{ $item->waveoff_resn}}</td>
                        <td>{{ $item->amount - $item->waveoff_amnt}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>


    <!--<input type="button" id="btnExport" class="btn btn-success" value=" Export Report To Excel " />-->

    <style>
        .select {
            margin-bottom: 30px;
            overflow: hidden;
        }

        table {
            background: #fff;
            margin: 0 0 1.5em;
            color: #76838f;
            border-collapse: collapse;
            border-spacing: 0;
            width: 100%;

        }

        th {
            border-color: #eaeaea;
            background: #f8fafc;
        }

        tr {
            line-height: 2.5;
        }

        table>thead>tr>th {
            border-bottom: 1px solid #e4eaec;
            border-right: 1px solid #e4eaec;
            text-align: center;
        }

        td {
            text-align: center;
            border-right: 1px solid #e4eaec;
            border-bottom: 1px solid #e4eaec;
        }

        table>tfoot>tr>th,
        table>thead>tr>th {
            font-weight: 400;
            color: #526069;
        }

        tr.sum {
            background: #ddd;
        }

    </style>
    
    
@section('javascript')
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

    <script>
        $(document).ready(function() {
            var title = "";
            @if (isset($_GET['to']) && $_GET['to'] > 0)
             title = "MYISD - Academy Report ({{ $_GET['from'] }} - {{ $_GET['to'] }})";
            @else
             title = "MYISD - Academy Report";
            @endif
            
            table = $('#dataTable').DataTable({
                dom: 'Bfrtip',
                destroy: true,
                filename:"test ",
                buttons: [
                    {
                        extend: 'excelHtml5',
                        title: title
                    }
                ]
            });

            // $("#btnExport").click(function(e) {
            //     window.open('data:application/vnd.ms-excel,' + $('#dvData').html());
            //     e.preventDefault();
            // });

        });
    </script>
@stop
@endsection
