@extends('voyager::master')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@section('page_header')
    <style>
        .view {
            margin-left: 4px;
        }

        .mb-3 {
            margin-bottom: 15px;
        }

    </style>
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="{{ $dataType->icon }}"></i> {{ $dataType->getTranslatedAttribute('display_name_plural') }}
        </h1>
        @can('add', app($dataType->model_name))
            <a href="{{ route('voyager.' . $dataType->slug . '.create') }}" class="btn btn-success btn-add-new">
                <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
            </a>
        @endcan
        @can('delete', app($dataType->model_name))
            @include('voyager::partials.bulk-delete')
        @endcan
        @can('edit', app($dataType->model_name))
            @if (isset($dataType->order_column) && isset($dataType->order_display_column))
                <a href="{{ route('voyager.' . $dataType->slug . '.order') }}" class="btn btn-primary btn-add-new">
                    <i class="voyager-list"></i> <span>{{ __('voyager::bread.order') }}</span>
                </a>
            @endif
        @endcan
        @if (!isset($_GET['not_paid']))
            <a href="{{ url('admin/bookings') }}?not_paid=1" class="btn btn-warning btn-add-new">
                <i class="voyager-plus"></i> <span>Show Unpaid Booking</span>
            </a>
        @else
            <a href="{{ url('admin/bookings') }}" class="btn btn-warning btn-add-new">
                <i class="voyager-plus"></i> <span>Show paid Booking</span>
            </a>
        @endif
        <a href="{{ url('admin/bookings') }}?payment=1" class="btn btn-info btn-add-new">
            <i class="voyager-plus"></i> <span>Payment Booking</span>
        </a>

        <div class="select">

            <form action="" method="get">
                <div class="col-sm-3">
                    <label for="staticEmail" class="col-sm-2 col-form-label">User</label>
                    <div class="col-sm-10">
                        <select class="livesearch form-control input-sm" name="payment">

                        </select>
                    </div>
                </div>
                <div class="col-sm-3">
                    <button type="submit" class="btn btn-primary">Show</button>
                </div>
            </form>
        </div>
    </div>

@stop

@section('content')
    <div class="page-content browse container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                    <tr>
                                        <th class="dt-not-orderable"><input type="checkbox" class="select_all"></th>
                                        <th>Client</th>
                                        <th>Book Code</th>
                                        <th>Book Date</th>
                                        <th>Book total</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                @foreach ($booking as $item)
                                    <tbody>
                                        <td><input type="checkbox" class="select_all" value="{{ $item->total }}"
                                                id="{{ $item->id }}"></td>
                                        <td>{{ $item->user->name }}</td>
                                        <td>{{ $item->code }}</td>
                                        <td>{{ $item->created_at }}</td>
                                        <td>{{ $item->total }} AED</td>
                                        <td></td>
                                    </tbody>
                                @endforeach
                                <tbody>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td id="total">0 AED</td>
                                    <td>
                                    </td>
                                </tbody>
                            </table>
                            @if ($_GET['payment'] > 1)
                                <form action="{{ url('book/pay') }}" method="post">
                                    @csrf
                                    <div class="mb-3">
                                        <label class="form-label"> Amount</label>
                                        <input type="text" class="form-control total" name="total" disabled>
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">Type</label>
                                        <select class="form-control" name="type">
                                            <option value="1">Cash</option>
                                            <option value="2">Bank Transfer</option>
                                            <option value="3">Client wallet</option>
                                            <option value="4">Credit Card</option>
                                            <option value="5">Cash / Credit Card</option>
                                        </select>
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">Transaction Authentication
                                            Code</label>
                                        <input type="text" class="form-control" name="ref">
                                    </div>
                                    <input type="hidden" name="ids" class="ids">
                                    <button type="submit" class="btn btn-success btn-add-new">Pay</button>
                                </form>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script>
        $('input:checkbox').change(function() {
            var total = 0;
            var ids = [];

            $('input:checkbox:checked').each(function() {
                total += isNaN(parseInt($(this).val())) ? 0 : parseInt($(this).val());
                ids.push($(this).attr("id"));
            });
            $(".ids").val(JSON.stringify(ids));
            $("#total").html(total + " AED");
            $(".total").val(total);
        });

        $('.livesearch').select2({
            placeholder: 'Select User',
            ajax: {
                url: '/ajax-autocomplete-users',
                dataType: 'json',
                delay: 250,
                processResults: function(data) {
                    return {
                        results: $.map(data, function(item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
    </script>
@stop
