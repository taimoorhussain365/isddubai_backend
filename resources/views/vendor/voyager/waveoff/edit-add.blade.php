@extends('voyager::master')

@section('css')

    <meta name="csrf-token" content="{{ csrf_token() }}">

@stop

@section('page_title', 'Academy Payment Wave-off')

@section('page_header')
    <h1 class="page-title"> <i class="voyager-dollar"></i>Academy Payment Wave-off 
    
        <a href="{{ url()->previous() }}" class="btn btn-warning">
            <span class="glyphicon glyphicon-list"></span>&nbsp;
            {{ __('voyager::generic.return_to_list') }}
        </a>

    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')

    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">

                    <form role="form" class="form-edit-add"
                        action="{{ route('waveoff.post', ['book_id' => $data['book_id'],'client_id' => $data['client_id']]) }}" method="POST">

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (Session::has('success'))
                                <div class="alert alert-success">
                                    {{ Session::get('success') }}
                                    @php
                                        Session::forget('success');
                                    @endphp
                                </div>
                            @endif

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <div class="form-group  col-md-12">
                                <label class="control-label" for="name">Academy Payment ID</label>
                                <input class="form-control" disabled value="{{ $data['book_id'] }}">
                            </div>

                            <div class="form-group  col-md-12">
                                <label class="control-label" for="refund_resn">Wave-off Amount</label>
                                <input type="text" class="form-control" name="waveoff_amnt" placeholder="0.00" value="{{ $pivote_academy_payment->waveoff_amnt ?? '' }}">
                            </div>

                            <div class="form-group  col-md-12">

                                <label class="control-label" for="refund_resn">Wave-off Amount Reason</label>
                                <textarea class="form-control" name="waveoff_resn" rows="5">{{ $pivote_academy_payment->waveoff_resn ?? '' }}</textarea>

                            </div>

                        </div>

                        <div class="panel-footer">
                        @section('submit-buttons')
                            <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                        @stop
                        <button type="button" onclick="goBack()"
                            class="btn btn-danger cancel">{{ __('voyager::generic.cancel') }}</button>
                        @yield('submit-buttons')
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>






@endsection

@section('javascript')
<script>
    function goBack() {
        window.history.back();
    }
</script>
@stop
