<ul class="pagination">
    @if ($paginator->onFirstPage())
        <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.first')">
            <span class="page-link" aria-hidden="true">&laquo;</span>
        </li>
    @else
        <li class="page-item">
            <a class="page-link" href="{{ \Request::url() }}" rel="prev"
                aria-label="@lang('pagination.first')">&laquo;</a>
        </li>
    @endif

    ...

    @if ($paginator->hasMorePages())
        <li class="page-item">
            <a class="page-link" href="{{ \Request::url() . '?page=' . $paginator->lastPage() }}" rel="last"
                aria-label="@lang('pagination.last')">&raquo;</a>
        </li>
    @else
        <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.last')">
            <span class="page-link" aria-hidden="true">&raquo;</span>
        </li>
    @endif
</ul>
