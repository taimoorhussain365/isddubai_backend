<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
</head>
<body>
<div>
    <p>Attention ISD Team:</p>
    <?php $training = App\Classes::find($class);
          $academy_name = App\Academy::find($academy)->value('title'); 
          $age_group = App\Age::find($training->age_group)->value('title');?>
    <p>Class {{$training->title}},</p>
    <p>...on Day <?=date('l')?>,</p>
    <p>...with Coach {{$training->coache ? $training->Coach->name : ''}} in {{$age_group}} age group</p>
    <p>In {{$academy_name}} Academy only has 2 spaces left.
    The class will no longer be available for clients when it reaches capacity
    Kindly create a new class by going to Academy > Training Times > Add New
    Or
    Edit the class to increase capacity by going to Academy > Training Times and selecting the class and pressing edit to increase its capacity.
    Thank you </p>
</div>
</body>
</html>