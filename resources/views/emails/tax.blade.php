<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Oswald&family=Roboto:wght@100&display=swap" rel="stylesheet">
    
    <style type="text/css">
        body {
            margin-top: 0px;
            margin-left: 0px;
            font-family: 'Roboto', sans-serif;        
        }

        #page_1 {
            position: relative;
            margin: 0px auto;
            padding: 0px;
            border: none;
            width: 640px
        }

        #page_1 #id1_1 {
            border: none;
            padding: 0px;
            border: none;
            width: 100%;
        }

        #page_1 #id1_2 {
            border: none;
            padding: 0px;
            border: none;
            width: 100%;
        }

        #page_1 #id1_2 #id1_2_1 {
            float: left;
            border: none;
            padding: 0px;
            border: none;
            width: 266px;
            overflow: hidden;
            margin-bottom: 18px;
        }

        #page_1 #id1_2 #id1_2_2 {
            float: right;
            border: none;
            margin: 1px 0px 0px 0px;
            padding: 0px;
            border: none;
            width: 50%;
            overflow: hidden;
        }

        #page_1 #id1_3 {
            background: #ebeaf0;
            margin-top: 3px;
            padding: 34px;
        }
        table.t1 td{
            text-align: center;
        }
        #page_1 #p1dimg1 {
            position: absolute;
            top: 0px;
            left: 0px;
            z-index: -1;
            width: 794px;
            height: 1079px;
        }

        #page_1 #p1dimg1 #p1img1 {
            width: 794px;
            height: 1079px;
        }




        .dclr {
            clear: both;
            float: none;
            height: 1px;
            margin: 0px;
            padding: 0px;
            overflow: hidden;
        }

        .ft0 {
            font: bold 33px;
            color: #f00d1f;
            line-height: 38px;

        }

        .ft1 {
            font: 16px 'Roboto';
            line-height: 18px;
            font-family: 'Roboto' !important;;

        }

        .ft2 {
            font: 11px 'Roboto';
            line-height: 14px;
            font-family: 'Roboto' !important;;

        }

        .ft3 {
            font: bold 12px 'Roboto';
            color: #ffffff;
            line-height: 15px;
        }

        .ft4 {
            font: 12px 'Roboto';
            line-height: 15px;
        }

        .ft5 {
            font: 12px 'Roboto';
            margin-left: 27px;
            line-height: 15px;
        }

        .ft6 {
            font: 12px 'Roboto';
            line-height: 30px;
        }

        .ft7 {
            font: bold 11px 'Roboto';
            color: #ffffff;
            line-height: 14px;
        }

        .ft8 {
            font: bold 10px 'Roboto';
            color: #ffffff;
            line-height: 12px;
        }

        .ft9 {
            font: 1px 'Roboto';
            line-height: 1px;
        }

        .ft10 {
            font: bold 8px 'Roboto';
            line-height: 10px;
        }

        .ft11 {
            font: 11px 'Roboto';
            line-height: 11px;
        }

        .ft12 {
            font: 8px 'Roboto';
            text-decoration: underline;
            line-height: 10px;
        }

        .ft13 {
            font: 8px 'Roboto';
            line-height: 10px;
        }

        .ft14 {
            font: 8px 'Roboto';
            margin-left: 1px;
            line-height: 10px;
            color: #000;
    text-align: left;
    margin-bottom: 20px;
        }

        .ft15 {
            font: bold 9px 'Roboto';
            line-height: 11px;
        }

        .ft16 {
            font: 9px 'Roboto';
            line-height: 12px;
        }

        .ft17 {
            font: 9px 'Roboto';
            line-height: 11px;
        }

        .ft18 {
            font: 12px 'Roboto';
            color: #fda914;
            line-height: 15px;
        }

        td {
            line-height: 3;
        }

        .p0 {
            text-align: left;
            margin-top: 0px;

        }

        .p1 {
            text-align: left;
            margin-top: 6px;

        }

        .p2 {
            text-align: left;
            margin-top: 4px;

        }

        .p3 {
            text-align: left;
            padding-left: 9px;
            margin-top: 35px;

        }

        .p4 {
            text-align: left;
            padding-left: 19px;
            margin-top: 29px;

        }

        .p5 {
            text-align: left;
            padding-left: 53px;
            padding-right: 142px;
            margin-top: 23px;

        }

        .p6 {
            text-align: left;
            padding-left: 53px;
            margin-top: 22px;

        }

        .p7 {
            text-align: right;
            margin-top: 0px;

        }

        .p8 {
            text-align: right;
            margin-top: 5px;

        }

        .p9 {
            text-align: right;
            margin-top: 4px;

        }

        .p10 {
            text-align: center;
            margin-top: 0px;

            white-space: nowrap;
        }

        span {
            text-align: center;
            display: block;
            color: #fff;
            line-height: 14px;
        }

        .p11 {
            text-align: center;
            margin-top: 0px;

            white-space: nowrap;
        }

        .p12 {
            text-align: center;
            padding-right: 19px;
            margin-top: 0px;

            white-space: nowrap;
        }

        .p13 {
            text-align: center;
            padding-left: 4px;
            margin-top: 0px;

            white-space: nowrap;
        }

        .p14 {
            text-align: left;
            margin-top: 0px;

            white-space: nowrap;
        }

        .p15 {
            text-align: center;
            padding-left: 5px;
            margin-top: 0px;

            white-space: nowrap;
        }

        .p16 {
            text-align: right;
            padding-right: 33px;
            margin-top: 0px;

            white-space: nowrap;
        }

        .p17 {
            text-align: right;
            padding-right: 31px;
            margin-top: 0px;

            white-space: nowrap;
        }

        .p18 {
            text-align: right;
            padding-right: 36px;
            margin-top: 0px;

            white-space: nowrap;
        }

        .p19 {
            text-align: right;
            padding-right: 88px;
            margin-top: 0px;

            white-space: nowrap;
        }

        .p20 {
            text-align: right;
            margin-top: 0px;

            white-space: nowrap;
        }

        .p21 {
            text-align: justify;
            padding-right: 76px;
            margin-top: 0px;

        }

        .p22 {
            text-align: justify;
            padding-right: 76px;
            margin-top: 5px;

        }

        .p23 {
            text-align: left;
            margin-top: 17px;

        }

        .p24 {
            text-align: left;
            padding-left: 200px;
            margin-top: 62px;

        }

        .p25 {
            text-align: left;
            padding-left: 163px;
            margin-top: 1px;

        }

        .td0 {
            padding: 0px;
            margin: 0px;
            width: 63px;
            vertical-align: bottom;
        }

        .td1 {
            padding: 0px;
            margin: 0px;
            width: 70px;
            vertical-align: bottom;
        }

        .td2 {
            padding: 0px;
            margin: 0px;
            width: 78px;
            vertical-align: bottom;
        }

        .td3 {
            padding: 0px;
            margin: 0px;
            width: 53px;
            vertical-align: bottom;
        }

        .td4 {
            padding: 0px;
            margin: 0px;
            width: 84px;
            vertical-align: bottom;
        }

        .td5 {
            padding: 0px;
            margin: 0px;
            vertical-align: bottom;
        }

        .td6 {
            padding: 0px;
            margin: 0px;
            width: 65px;
            vertical-align: bottom;
        }

        .tr0 {
            height: 18px;
        }

        .tr1 {
            height: 16px;
        }


        .tr3 {
            height: 19px;
        }

        .tr4 {
            height: 23px;
        }

        .tr5 {
            height: 22px;
        }

        .t0 {
            width: 100%;
            margin-top: 120px;
            font: bold 10px;
            color: #ffffff;
            border-collapse: separate;
            border-spacing: 0 2px;
        }

        .t1 {
            width: 100%;
            font: 12px 'Roboto';
            background: #ebeaf0;
            padding: 21px;
        }

        .tr_bg {
            background: red;
            height: 30px;
            line-height: 30px;
        }

        .tr0 {
            height: 30px;
            border-right: solid 1px #fff;
            vertical-align: top;
            padding-top: 4px;
        }

        .tr0 p {
            font-size: 13px;
            line-height: 15px;
            margin-bottom: 0;
        }

        tr {
            background: #ebeaf0;
            color: #000;
        }

        p.p10.ft4 {
            text-align: left;
            line-height: 3;
        }
        .p10,.tr2 span {
            color: #000;
            line-height: 4;
            font-size: 13px;            
        }
        td.tr2 {
            vertical-align: middle;
        }        
        .footer {
            background: #3a2c69;
            text-align: center;
            padding: 10px 0px;
        }

        div#logo {
            float: right;
            margin-bottom: 40px;
        }
        .tt1 span {
            color: #000;
            font-family: 'Roboto', sans-serif; 
        }
        .tt2 td span {
            line-height: 3;
            color: #fff;
        } 
        .id1_2_1 span{
             color: #000;
            text-align: left;
            line-height: 2;
        }        
        .tt2 td{
            font-family: 'Roboto', sans-serif;  
        }     
        #id1_3 span {
            color: #000;
            text-align: left;
            font-size: 12px;
            line-height: 1.6;
        }     
        .footer span {
            line-height: 1.5;
            font-size: 14px;
        }     
        .tt1 td {
    height: 30px;
}                
    </style>
</HEAD>

<body>
    <div id="page_1">
        <?php
        $users = DB::table('users')->where('id', $details['client_id'])->first();
        $vat = $details['total'] - round($details['total'] / 1.05);
        $subtotal = $details['total'] - $vat;
        ?>

        <div id="id1_2">

            <table style="width:100%">
                <tr style="background: transparent">
                    <td>
                        <P class="p0 ft0" style="font-family: 'Oswald' !important;font-size: 33px;">TAX INVOICE</P>
                    </td>
                    <td style="direction: rtl;">
                        <img src="https://bookings.isddubai.com/public//storage/settings/June2021/384dQ9eJeCJTfa6TVUWE.png" alt="" width="150" srcset="">
                    </td>
                </tr>
            </table>
            <div id="id1_2_1" class="id1_2_1">
                <span class="">{{ $users->name }}</span>
                <span class="">Email: {{ $users->email }}</span>
                <span class="">Contact Number: {{ $users->contacts }}</span>
            </div>
            <div id="id1_2_2" class="id1_2_1">
                <span class="">Invoice Date: {{ $details['created_at']->format('d/m/Y') }}</span>
                <span class="">Invoice# <NOBR>FC-{{ $details['code'] }}</NOBR>
                </span>
                <span class="">Payment terms: Immediate</span>
            </div>
            <table cellpadding=0 cellspacing=0 class="t0">
                <tr style="background: red">
                    <td class="tr0 td0">
                        <span>S.No</span>
                    </td>
                    <td class="tr0 td0">
                        <span>Description</span>
                    </td>
                    <td class="tr0 td0">
                        <span>Duration</span>
                        <span>(hrs)</span>
                    </td>
                    <td class="tr0 td1">
                        <span>Unit Price</span>
                    </td>
                    <td class="tr0 td2">
                        <span class=" ">Gross Total</span>
                        <span>AED</span>
                    </td>
                    <td class="tr0 td3">
                        <span class=" ">VAT %</span>
                    </td>
                    <td class="tr0 td4">
                        <span class=" ">VAT amount</span>
                    </td>
                    <td class="tr0 td3">
                        <span class=" ">Net Total</span>
                        <span>AED</span>
                    </td>
                </tr>
                <tr>
                    <td class="tr2 td0">
                        <span class="p10 ">1</span>
                    </td>
                    <td class="tr2 td0">
                        <span class="">sport</span>
                    </td>
                    <td class="tr2 td0">
                        <span class="p10 ">1</span>
                    </td>
                    <td class="tr2 td1">
                        <span class="p16 ft4">{{ $subtotal }}</span>
                    </td>
                    <td class="tr2 td2">
                        <span class="">{{ $subtotal }} AED</span>
                    </td>
                    <td class="tr2 td3">
                        <span class="p17 ft4">5</span>
                    </td>
                    <td class="tr2 td4">
                        <span class="p18 ft4">{{ $vat }}</span>
                    </td>
                    <td class="tr2 td3">
                        <span class="">{{ $details['total'] }}</span>
                    </td>
                </tr>
                <tr>
                    <td class="tr2 td0" colspan="3">
                        <span class="">Pitch / Court:</span>
                    </td>                    </td>
                    <td class="tr2 td3" colspan="5">
                        <span class="">{{$details['pitch']}}</span>
                    </td>
                </tr>
                <tr>
                    <td class="tr2 td0" colspan="3">
                        <span class="" >Date:</span>
                    </td>
                    <td class="tr2 td3" colspan="5">
                        <span class="">{{$details['date']}}</span>
                    </td>
                </tr>
                <tr>
                    <td class="tr2 td0" colspan="3">
                        <span class="" >Time:</span>
                    </td>
                    <td class="tr2 td3" colspan="5">
                        <span class="">{{$details['from']}} - {{$details['to']}}</span>
                    </td>
                </tr>
            </table>
            <table cellpadding=0 cellspacing=0 class="t1 tt1">
                <tr>
                    <td class="tr3 td5">
                        <span class="">Subtotal:</span>
                    </td>
                    <td class="tr3 td6">
                        <span class="">{{ $subtotal }} AED </span>
                    </td>
                </tr>
                <tr>
                    <td class="">
                        <span class=" ">5% VAT:</span>
                    </td>
                    <td class="">
                        <span class="">+ {{ $vat }} AED</span>
                    </td>
                </tr>
                <tr>

                    <td class="">
                        <span class=" ">Total:</span>
                    </td>
                    <td class="">
                        <span class=" ">{{ $details['total'] }} AED</span>
                    </td>
                </tr>
                <tr>
                    <td class="">
                        <span class=" " >Payment Received:</span>
                    </td>
                    <td class="tr4 td6">
                        <?php $received = DB::table('payment')->where('book_id', $details['id'])->sum('amount');?>
                        <span class=" " > - <?=$received?> AED</span>
                    </td>
                </tr>
                <tr>

                    <td class="">
                        <span class=" " >Balance due:</span>
                    </td>
                    <td class="">
                        <span class=" ">{{ $details['total'] - $received}} AED</span>
                    </td>
                </tr>
            </table>

            <table cellpadding=0 cellspacing=0 class="t1 tt2">
                <tr style="background: red">
                    <td><span>#</td>
                    <td><span>Payment Date</span></td>
                    <td><span>Payment Sub Total</td>
                    <td><span>VAT</td>
                    <td><span>Payment Grand Total</td>
                </tr>
                <?php $payments = DB::table('payment')->where('book_id', $details['id'])->get() ?>
                @foreach($payments as $payment)
                <tr>
                    <td>1</td>
                    <td>{{$payment->created_at}}</td>
                    <td><?= round($payment->amount / 1.05) ?></td>
                    <td><?= $payment->amount - round($payment->amount / 1.05) ?></td>
                    <td>{{$payment->amount}}</td>
                </tr>    
                @endforeach
            </table> 

        </div>
        <div id="id1_3">
            <span class="">
                Groups are capped to 7 aside maximum.Maximum allowed time is <br/>
                Entry and exit will be from the front main entrance of
                the football center only.You must wear a mask when entering and
                have your
                temperature checked. • Only 1 single cashless payment will be accepted (no cash/Multiple cards). 
                No bibs or balls will be provided. <br>
                Water will be
                available to purchase as normal.Changing room facilities will
                not be
                available.No spectators allowed. Please arrive
                on time and leave directly after the game, no gatherings allowed in the facility<br>
                If you are late we will not be able to provide you extra time as there is a short gap between
                bookings and there is no option of overlapping groups.The
                Football Center
                and DSC management will not be responsible for your personal belongings and minor/major injuries at the
                facility.In case of any changes/cancellations from The Football
                Center
                senior management or the customer, notice must be given at least <br>The payment must be made at the
                front desk
                before your game starts by the organizer.Fees are not refundable
                under any
                cancellation circumstances.Late cancellation fee of 150 AED will
                be
                charged.</span>
            <span class="">The Organizer reserves the right
                not to permit the
                access and remove any Player who behaves improperly or abusively towards him/herself or otherpersons,
                has been involved in a fraudulent activity or otherwise interferes with the normal running of the
                Academy. No refund will be given should the Player be removed from the Academy for these reasons. The
                Organizer shall not be liable to the Player and the Player’s Parents / Guardians for any force majeure
                event or occurrence or any Organizer’saction done based on a legal or regulatory imperative, including
                the suspension or cancellation of the Academy Activities. The Organizer and DSC disclaims any liability
                in relation to Players’ claims and</span>
            <span class=" "><span class="">/</span><span class="">or third
                    parties’ claims that may arise as
                    a result of the views and /or statements expressed and / or the use of images and / or personal data
                    that could arise in relation to the Players that may appear in the massmedia. The Football Center
                    and DSC are exempt of any liability in regard to the Academy, which is exclusively the
                    responsibility of the Organizer. I acknowledge the contagious nature of </span>
                <NOBR>COVID-19</NOBR> and understand the risk of becoming exposed to, or infected by, <NOBR>COVID-19
                </NOBR> at the sports facility.On my behalf, and on behalf of my children, I hereby covenant not to sue,
                hold harmless, and release and discharge LaLiga, the Organizer, theFacility and the facility management
                company as well as their successors, employees, agents and representatives, of all liabilities, claims,
                actions,damages, costs or expenses of any kind arising out thereof, or relating thereto, whether a
                <NOBR>COVID-19</NOBR> infection occurs, before, during or afterparticipating in events and activities
                organized by LaLiga or the Organizer.
            </span>
            <span class=" ">Payment terms:</span>
            <span class=" ">Upon conﬁrmation</span>
            <span class=" ">Cash / Check in favor of: Inspiratus Consulting Limited Dubai Br</span>
            <span class=" ">Bank Details: Emirates NBD, Account Name: Inspiratus Consulting Limited, SWIFT
                Code:
                EBILAEAD, AED Account Number: <NOBR>03-4803</NOBR>
                <NOBR>4845-101,</NOBR> AED IBAN
            </span>
            <span class=" ">Number: AE370260001014845480303, trN # 100268867700003</span>
        </div>
        <div class="footer">
            <span class=" ">Inspiratus Sports District, Dubai Sports City</span>
            <span class=" ">04 4481 - www.isddubai.com - sports@isddubai.com</span>
        </div>
    </div>
</body>

</html>
