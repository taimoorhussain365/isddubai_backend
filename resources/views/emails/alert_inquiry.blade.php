<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
</head>
<body>
<div>
    <?php $subscription = App\Subscription::find($subscription);
          $academy_name = App\Academy::find($academy)->value('title'); 
          $Parent = App\User::where('id',$subscription->user_id)->select('name', 'contacts', 'email')->first(); 
          $child = ""; 
          ?>
        {{$Parent->name}} has placed an inquiry for {{$academy_name}}. Child name is {{$child}}, Child DOB is X, 
        Number of training days requested is {{$subscription->num_days}}, requested start date is {{$subscription->start_date}} for {Term selection}. 
        Contact number is {{$Parent->contacts}}, email address is {{$Parent->email}}. 
        Please contact them within 24 hours to complete their registration process. 
        Their booking is available for review in Academy>Subscriptions
</div>
</body>
</html>