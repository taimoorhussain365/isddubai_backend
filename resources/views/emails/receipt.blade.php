<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<HTML>

<HEAD>
    <META http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <TITLE>pdf-html</TITLE>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100&display=swap" rel="stylesheet">
    <META name="generator" content="BCL easyConverter SDK 5.0.252">
    <META name="title" content="Payment_Receipt">
    <STYLE type="text/css">
        body {
            margin-top: 0px;
            margin-left: 0px;
            font-family: 'Roboto';
        }

        #page_1 {
            position: relative;
            margin: 20px auto;
            padding: 0px;
            border: none;
            width: 794px;
        }

        #id1_1 p.p0.ft0 {
            text-align: left;
        }

        #page_1 #id1_1 {
            border: none;
            margin: 0px;
            padding: 0px;
            border: none;
            overflow: hidden;
        }

        #page_1 #id1_2 {
            border: none;
            margin: 24px 0px 0px 0px;
            padding: 0px;
            border: none;
            width: 88%;
            overflow: hidden;
        }

        #page_1 #id1_3 {
            border: none;
            margin: 0px auto;
            padding: 15px;
            margin-top: 22px;
            border: none;
            overflow: hidden;
            background: #3a2c69;

        }

        #page_1 #p1dimg1 {
            position: absolute;
            top: 0px;
            left: 0px;
            z-index: -1;
            width: 794px;
            height: 529px;
        }

        #page_1 #p1dimg1 #p1img1 {
            width: 794px;
            height: 529px;
        }




        .dclr {
            clear: both;
            float: none;
            height: 1px;
            margin: 0px;
            padding: 0px;
            overflow: hidden;
        }

        .ft0 {
            font: bold 29px 'Arial';
            color: #f00f20;
            line-height: 34px;
        }

        .ft1 {
            font: bold 16px 'Arial';
            line-height: 19px;
        }

        .ft2 {
            font: 16px 'Arial';
            line-height: 18px;
        }

        .ft3 {
            font: bold 26px 'Arial';
            color: #fda914;
            line-height: 30px;
        }

        .ft4 {
            font: 12px 'Arial';
            color: #fda914;
            line-height: 15px;
        }

        .p0 {
            text-align: center;
            margin-top: 0px;
            margin-bottom: 5px;
        }

        .p1 {
            text-align: left;
            padding-left: 84px;
            margin-top: 0px;
            margin-bottom: 0px;
        }

        .p2 {
            text-align: left;
            padding-left: 10px;
            margin-bottom: 0px;
        }

        .p3 {
            text-align: center;
            padding-left: 0px;
            margin-top: 30px;
            margin-bottom: 30px;
        }

        .p4 {
            text-align: right;
            padding-right: 198px;
            margin-top: 55px;
            margin-bottom: 0px;
        }

        .p5 {
            text-align: center;
            padding-left: 0px;
            margin-top: 10px;
            margin-bottom: 0px;
        }

        p.p2 {
            background: #ebeaf0;
            line-height: 40px;
            height: 40px;

            margin-bottom: 5px;
            margin-top: 0px;
        }

    </STYLE>
</HEAD>

<BODY>
    <div id="page_1">
        <div id="id1_1">
            <P class="p0 ft0">PAYMENT RECEIPT</P>
        </div>
        <div id="id1_2">
            <P class="p2 ft2"><SPAN class="ft1">Name: </SPAN>{{ $details['name'] }}</P>
            <P class="p2 ft2"><SPAN class="ft1">Email: </SPAN>{{ $details['email'] }}</P>
            <P class="p2 ft1">Invoice Number: <NOBR><SPAN class="ft2">{{ $details['code'] }}</SPAN>
                </NOBR>
            </P>
            <P class="p2 ft1">Amount Received: <SPAN class="ft2">{{ $details['total'] }} AED</SPAN>
            </P>
            <P class="p2 ft1">Date of Payment: <SPAN class="ft2">
                    {{ \Carbon\Carbon::parse($details['date'])->format('d M Y') }}</SPAN></P>
            <P class="p2 ft2"><SPAN class="ft1">Payment Method: </SPAN>Online (credit card)
            </P>
            <P class="p3 ft3">THANK YOU FOR YOUR PAYMENT!</P>
        </div>
        <div id="id1_3">
            <P class="p5 ft4">Inspiratus Sports District, Dubai Sports City</P>
            <P class="p0 ft4">04 4481555 - www.isddubai.com - sports@isddubai.com</P>
        </div>
    </div>
</BODY>

</HTML>
