<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@400;500;600;700&display=swap&Roboto:wght@100&display=swap" rel="stylesheet">
    <style>     
        body {
            font-family: 'roboto' !important;
            padding: 0px 30px;
        }

        h1,
        h4 {
            font-family: 'Oswald', sans-serif !important;
        }

        img {
            margin: 35px auto;
            display: block;
        }

        h4 {
            font-family: 'Oswald', sans-serif !important;
            color: red;
            font-size: 20px;
            margin-bottom: 0;
        }

        li {
            list-style-type: none;
            font-size: 16px;
            font-weight: bolder;
            color: #3E2B64;
            line-height: 1.5;
            font-family: 'Roboto', 'sans-serif';
        }

        ul {
            padding: 0;
        }

        footer {
            text-align: center;
            font-weight: bold;
        }

        p {
            margin-top: 0;
            margin-bottom: 5px;
        }

        span {
            margin-left: 10px;
        }

    </style>
</head>

<body>
    <div>
        <table style="width:100%">
        <tr style="margin: 0 auto;text-align: center;">
            <td>
                <div style="margin: 0 auto; width: 150">
                    <img src="https://bookings.isddubai.com/public/logo2.png"  width="150">
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="margin-top:150px;">
                    <h1 style="font-family: 'Oswald', sans-serif !important;text-align: center;color: #ffb938;font-size: 48px;letter-spacing: .8px;">TAX INVOICE</h1>
                </div>
            </td>
        </tr>
        </table>
        <h4>CUSRTOMER DETAILS</h4>
        <ul  style="font-family: 'roboto', sans-serif !important;">
            <?php $user = App\User::find($details['client_id']);
            $Booking = App\Booking::find($details['id']); ?>
            <li>Customer Name: <span>{{ $user->name }}</span></li>
            <li>Customer Email Address:<span>{{ $user->email }}</span> </li>
            <li>Customer Mobile No:: <span>{{ $user->contacts }}</span></li>
            <li>Invoice Number : <span>{{ $Booking['code'] }}</span></li>
        </ul>

        <h4>BOOKING DETAILS</h4>
        <ul  style="font-family: 'roboto', sans-serif !important;">
            <?php $sports = DB::table('sports')->where('id', $Booking['sports'])->value('name'); ?>
            <li>Sport: <span>{{ $sports }}</span></li>
            <li>Pitch/Court: <span>{{ $Booking['pitch'] }}</span></li>
            <li>Date:Time: <span>{{ $Booking['date'] }}</span></li>
            @if($Booking['sports'] != 5)
            <li>Duration: <span>{{ $Booking['from'] }} / {{ $Booking['to'] }}</span></li>
            @endif
            <?php $sum = DB::table('payment')->where('book_id', $Booking['id'])->sum('amount');
            $type = DB::table('payment')->where('book_id', $Booking['id'])->value('type'); ?>
            <li>Discount:<span>{{ $Booking['dicsount'] }}</span></li>
            <?php
            if($type == 1){
                $type = "Cash";
            }elseif ($type == 2) {
                $type = "Bank Transfer";
            }elseif ($type == 3) {
                $type = "Client wallet";
            }elseif ($type == 4) {
                $type = "Credit Card";
            }elseif ($type == 5) {
                $type = "Cash / Credit Card";
            }else{
                $type = "other";
            }
            ?>
            <li>Paymet Method: <span>{{$type}}</span></li>
        </ul>

        <h4>AMOUNT BILLED</h4>
        <ul  style="font-family: 'roboto', sans-serif !important;">
            
            <li>Sub Total: <?= round($sum / 1.05) ?></li>
            <li>VAT: <?= $sum - round($sum / 1.05) ?></li>
            <li>Grand Total : {{ $sum }}</li>
        </ul>
        <h1 style="text-align: center;color: #ffb938;    font-size: 25px;letter-spacing: .8px;">THANK YOU FOR YOUR
            PAYMENT!</h1>
        <footer style="font-family: 'roboto', sans-serif !important;    font-size: 13px;">
            <p>Inspiratus Sports District, Dubai Sports City</p>
            <p>04 448 1555 - www.isddubai.com - sports@isddubai.com</p>
        </footer>
    </div>
</body>

</html>
