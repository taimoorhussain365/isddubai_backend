<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
     return redirect('admin');;
});

// Route::get('/active', function () {
//     $update=  DB::table('users')->where('id', $_GET['id'])->where('email', $_GET['email'])->where('activation_code', $_GET['code'])->update(['status' => 1]);
//     if($update){
//         //Mail::to($_GET['email'])->send(new \App\Mail\Welcom());        
//         return "your Account is Active Now";
//     }else{
//      return "Error Active You Account";
//     }
// });

 
Route::get('/delete/{id}', 'TimesController@DeleteTime');
Route::get('/check/courts/{id}', 'CourtsController@CheckTime');
Route::get('/check/products/{id}', 'BookingsController@CheckProduct');
Route::post('/check/sport', 'CourtsController@CheckSport');
Route::post('/check/book', 'BookingsController@store');
Route::post('/edit/book', 'BookingsController@edite');
Route::post('/add/pay', 'BookingsController@AddPay');
Route::get('/export/invoice/{id}', 'BookingsController@ExportInvoice');
Route::get('/export/receipt/{id}', 'BookingsController@ExportReceipt');
Route::get('/export/receipt/subscription/{id}', 'SubscriptionsController@ExportReceiptSubscription');
Route::get('/export/product/invoice/{id}', 'BookingsController@ExportInvoiceProduct');
Route::get('/order/product/{id}', 'BookingsController@DeleteOrderProduct');
Route::get('/backup','AdminController@backup');
Route::get('/active','BookingsController@active');
Route::post('/book/pay', 'BookingsController@BookingPay');
Route::get('/ajax-autocomplete-users','BookingsController@Autocomplete');
Route::post('/comment/add', 'AdminController@CommentAdd');
Route::post('/admin/coache/send/mail', 'AdminController@SendEmailCoache');
// Route::get('/academy_payment', 'AcademyPaymentController@index')->name("academypayment");



Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();

    Route::get('/options/', 'RefundController@index')->name('refund.index');
    Route::post('/options/{book_id}/{client_id}', 'RefundController@post')->name('refund.post');

    // refund 
    // Route::get('/refund/', 'RefundController@index')->name('refund.index');
    // Route::post('/refund/{book_id}/{client_id}', 'RefundController@post')->name('refund.post');

    // waveoff
    Route::get('/waveoff/', 'WaveoffController@index')->name('waveoff.index');
    Route::post('/waveoff/{book_id}/{client_id}', 'WaveoffController@post')->name('waveoff.post');

    Route::get('/wallets', 'WalletController@index')->name('voyager.wallets.index');
    Route::get('/wallets/create/{client_id}', 'WalletController@create')->name('voyager.wallets.create');
    Route::post('/wallets/store/', 'WalletController@store')->name('voyager.wallets.store');
    
    Route::get('/reports/booking', 'ReportsController@Booking');
    Route::get('/reports/schedule', 'ReportsController@Schedule');
    Route::get('/reports/finance', 'ReportsController@Finance');
    Route::get('/reports/academy', 'ReportsController@Academy');
    Route::get('/reports/academy/refund-reason', 'ReportsController@getRefundReason')->name('refund.reason.history');

    
    Route::get('/subscription/publish/customer/{user}/{id}', 'SubscriptionsController@PublishCustomer');
    
    


});