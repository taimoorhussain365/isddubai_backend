<?php
/**
 * @copyright Copyright PayFort 2012-2016 
 * 
 */



class PayfortIntegration
{

   
    public $gatewayHost        = 'https://checkout.payfort.com/';
    public $gatewaySandboxHost = 'https://sbcheckout.payfort.com/';
    public $language           = 'en';
    /**
     * @var string your Merchant Identifier account (mid)
     */
    public $merchantIdentifier = 'LmIAIBAK';
    
    /**
     * @var string your access code
     */
    public $accessCode         = 'GktW1EwizGB7tZ51ocEG';
    
    /**
     * @var string SHA Request passphrase
     */
    public $SHARequestPhrase   = '83/cPmhQ/ub0O3i8z0Qp4j-[';
    
    /**
     * @var string SHA Response passphrase
     */
    public $SHAResponsePhrase = '64WSYHztai84ouu6X5/VpX-)';
    
    /**
     * @var string SHA Type (Hash Algorith)
     * expected Values ("sha1", "sha256", "sha512")
     */
    public $SHAType       = 'sha256';
    
    /**
     * @var string  command
     * expected Values ("AUTHORIZATION", "PURCHASE")
     */
    public $command       = 'PURCHASE';
    
    /**
     * @var decimal order amount
     */
    public $amount;
    
    /**
     * @var string order currency
     */
    //public $currency           = 'SAR';

    public $currency;
    public $id;
    public $client_id;
    public $rent;

    /**
     * @var string item name
     */
    public $itemName           = 'Apple iPhone 6s Plus';
    
    /**
     * @var string you can change it to your email
     */
    //public $customerEmail      = 'test@test.com';

    public $customerEmail ;
    public $customerName ;
    
    /**
     * @var boolean for live account change it to false
     */
    public $sandboxMode        = true;
    /**
     * @var string  project root folder
     * change it if the project is not on root folder.
     */
    public $projectUrlPath     = '/paymobileapp'; 
    public $projectUrlPath_     = '/'; 
	

    public function __construct()
    {
        $this->currency = "AED";
        $this->customerEmail = 'mahmoudsehsah@gmail.com';
        $this->customerName = 'cName';       
        $this->id = $_GET["id"]; 
        $this->amount = $_GET["amount"]; 
        $this->client_id = $_GET["client_id"]; 
        $this->rent = $_GET["rent"]; 

    } 

    public function processRequest($paymentMethod)
    {
        
        if ($paymentMethod == 'cc_merchantpage' || $paymentMethod == 'cc_merchantpage2' || $paymentMethod == 'installments_merchantpage') {
            $merchantPageData = $this->getMerchantPageData($paymentMethod);
            $postData = $merchantPageData['params'];
            $gatewayUrl = $merchantPageData['url'];
        }
        else{
            $data = $this->getRedirectionData($paymentMethod);
            $postData = $data['params'];
            $gatewayUrl = $data['url'];
        }
        $form = $this->getPaymentForm($gatewayUrl, $postData);
        echo json_encode(array('form' => $form, 'url' => $gatewayUrl, 'params' => $postData, 'paymentMethod' => $paymentMethod));
        exit;
    }

    public function getRedirectionData($paymentMethod) {
        session_start();
        $amount = $_SESSION["amount"]; 
        $merchantReference = $this->generateMerchantReference();
        if ($this->sandboxMode) {
            $gatewayUrl = $this->gatewaySandboxHost . 'FortAPI/paymentPage';
        }
        else {
            $gatewayUrl = $this->gatewayHost . 'FortAPI/paymentPage';
        }

        if ($paymentMethod == 'sadad') {
            $this->currency = 'SAR';
        }
        $postData = array(
            'amount'              => $this->convertFortAmount(600, $this->currency),
            'currency'            => strtoupper($this->currency),
            'merchant_identifier' => $this->merchantIdentifier,
            'access_code'         => $this->accessCode,
            'merchant_reference'  => $merchantReference,
            'customer_email'      => 'test@payfort.com',
            //'customer_name'         => trim($order_info['b_firstname'].' '.$order_info['b_lastname']),
            'command'             => $this->command,
            'language'            => $this->language,
            'return_url'          => $this->getUrl('route.php?r=processResponse'),
        );

        if ($paymentMethod == 'sadad') {
            $postData['payment_option'] = 'SADAD';
        }
        elseif ($paymentMethod == 'naps') {
            $postData['payment_option']    = 'NAPS';
            $postData['order_description'] = $this->itemName;
        }
        elseif ($paymentMethod == 'installments') {
            $postData['installments']    = 'STANDALONE';
            $postData['command']         = 'PURCHASE';
        }
        $postData['signature'] = $this->calculateSignature($postData, 'request');
        $debugMsg = "Fort Redirect Request Parameters \n".print_r($postData, 1);
        $this->log($debugMsg);
        return array('url' => $gatewayUrl, 'params' => $postData);
    }
    
    public function getMerchantPageData($paymentMethod)
    {
        session_start();
        $amount = $_SESSION["amount"];           
        $merchantReference = $this->generateMerchantReference();
        $returnUrl = $this->getUrl('route.php?r=merchantPageReturn');
        if(isset($_GET['3ds']) && $_GET['3ds'] == 'no') {
            $returnUrl = $this->getUrl('route.php?r=merchantPageReturn&3ds=no');
        }
        $iframeParams              = array(
            'merchant_identifier' => $this->merchantIdentifier,
            'access_code'         => $this->accessCode,
            'merchant_reference'  => $merchantReference,
            'service_command'     => 'TOKENIZATION',
            'language'            => $this->language,
            'return_url'          => $returnUrl,
        );
        
        if($paymentMethod == 'installments_merchantpage'){
                $iframeParams['currency']       = strtoupper($this->currency);
                $iframeParams['installments']   = 'STANDALONE';
                $iframeParams['amount']         = $this->convertFortAmount(600, $this->currency);
        }
        
        
        $iframeParams['signature'] = $this->calculateSignature($iframeParams, 'request');

        if ($this->sandboxMode) {
            $gatewayUrl = $this->gatewaySandboxHost . 'FortAPI/paymentPage';
        }
        else {
            $gatewayUrl = $this->gatewayHost . 'FortAPI/paymentPage';
        }
        $debugMsg = "Fort Merchant Page Request Parameters \n".print_r($iframeParams, 1);
        $this->log($debugMsg);
        
        return array('url' => $gatewayUrl, 'params' => $iframeParams);
    }
    
    public function getPaymentForm($gatewayUrl, $postData)
    {
        $form = '<form style="display:none" name="payfort_payment_form" id="payfort_payment_form" method="post" action="' . $gatewayUrl . '">';
        foreach ($postData as $k => $v) {
            $form .= '<input type="hidden" name="' . $k . '" value="' . $v . '">';
        }
        $form .= '<input type="submit" id="submit">';
        return $form;
    }

    public function processResponse()
    {
        session_start();
        $id = $_SESSION["order_id"];
        $client_id = $_SESSION["client_id"];
        $rent = $_SESSION["rent"];

        $fortParams = array_merge($_GET, $_POST);
        $debugMsg = "Fort Redirect Response Parameters \n".print_r($fortParams, 1);
        $this->log($debugMsg);

        $reason        = '';
        $response_code = '';
        $success = true;
        if(empty($fortParams)) {
            $success = false;
            $reason = "Invalid Response Parameters";
            $debugMsg = $reason;
            $this->log($debugMsg);
        }
        else{
            //validate payfort response
            $params        = $fortParams;
            $responseSignature     = $fortParams['signature'];
            $merchantReference = $params['merchant_reference'];
            unset($params['r']);
            unset($params['signature']);
            unset($params['integration_type']);
            $calculatedSignature = $this->calculateSignature($params, 'response');
            $success       = true;
            $reason        = '';

            if ($responseSignature != $calculatedSignature) {
                $success = false;
                $reason  = 'Invalid signature.';
                $debugMsg = sprintf('Invalid Signature. Calculated Signature: %1s, Response Signature: %2s', $responseSignature, $calculatedSignature);
                $this->log($debugMsg);
            }
            else {
                $response_code    = $params['response_code'];
                $response_message = $params['response_message'];
                $status           = $params['status'];
                if (substr($response_code, 2) != '000') {
                    $success = false;
                    $reason  = $response_message;
                    $debugMsg = $reason;
                    $this->log($debugMsg);
                    $return_url = $this->getUrl1('api/v1/mobilePaymentDeclined?id='.$id.'&error='.$params);
                    return header('Location: '.$return_url);
                    exit;

                }
            }
        }
            $servername = "127.0.0.1";
            $username = "isddubai_isd";
            $password = "isddubai_isd";
            $dbname = "isddubai_isd";
            $conn = new mysqli($servername, $username, $password, $dbname);
        if(!$success) {
            echo $params;
            $p = $params;
            $p['error_msg'] = $reason;
            $res=  http_build_query($params);
            parse_str($res, $output);
            $card_number = $output['card_number'];
            //$amount = $output['amount'];
            $merchant_reference = $output['merchant_reference'];
            $desc = "Payment Failed ".$amount."by visa Number ".$card_number;              
            $sql = "UPDATE bookings SET status='2' WHERE id=".$id;
            $sql2 = "INSERT INTO payment_log (log, book_id) VALUES ('$res', '$id')";  
            $sql3 = "INSERT INTO payment (amount,type, book_id,client_id,ref,status,description,rent) VALUES ('$amount', '0', '$id', '$client_id', '$merchant_reference', '2','$desc','$rent')";  
            $conn->query($sql);
            $conn->query($sql2);
            $conn->query($sql3);
            $conn->close();  
            $return_url = $this->getUrl1('api/v1/mobilePaymentFail?id='.$id.'&error='.$params);

        }
        else{
            $res=  http_build_query($params);
            parse_str($res, $output);
            $card_number = $output['card_number'];
            $merchant_reference = $output['merchant_reference'];
            $token  = $output['token_name'];
            $expiry_date = $output['expiry_date'];            
            $desc = "Payment Successful ".$amount."by visa Number ".$card_number;              
            $sql3 = "INSERT INTO payment (amount,type, book_id,client_id,ref,status,description,rent) VALUES ('$amount', '0', '$id', '$client_id', '$merchant_reference', '1','$desc','$rent')";  
            $sql = "UPDATE bookings SET status='1' WHERE id=".$id;
            $sql2 = "INSERT INTO payment_log (log, book_id) VALUES ('$res', '$id')";    
            $conn->query($sql);
            $conn->query($sql2);
            $conn->query($sql3);
            $fortParams = array_merge($_GET, $_POST);
            $fortParams["remember_me"];
            
            $result = "SELECT * FROM cards WHERE card_number LIKE '".$card_number."%' AND user = '".$client_id."' ";
            $results = mysqli_query($conn, $result);
            $rows = mysqli_num_rows($results);
            if ($rows > 0) {
            }else{
                if($fortParams["remember_me"] == "YES"){
                    $sql4 = "INSERT INTO cards (token, user, card_number, card_expire) VALUES ('$token' , '$client_id','$card_number' ,'$expiry_date')";  
                    $conn->query($sql4);
                }
            }
             $return_url = $this->getUrl1('api/v1/mobilePaymentSuccess?'.http_build_query($debugMsg).'&id='.$id);
        }
       
        return header('Location: '.$return_url);
        //echo "<html><body onLoad=\"javascript: window.top.location.href='" . $return_url . "'\"></body></html>";
        exit;
    }

    public function processMerchantPageResponse()
    {
        session_start();
        $id = $_SESSION["order_id"];
        $client_id = $_SESSION["client_id"];
        $rent = $_SESSION["rent"];

        $fortParams = array_merge($_GET, $_POST);

        $debugMsg = print_r($fortParams, 1);
        $this->log($debugMsg);
        $reason = '';
        $response_code = '';
        $success = true;
        if(empty($fortParams)) {
            $success = false;
            $reason = "Invalid Response Parameters";
            $debugMsg = $reason;
            $this->log($debugMsg);
        }
        else{
            //validate payfort response
            $params        = $fortParams;
            $responseSignature     = $fortParams['signature'];
            unset($params['r']);
            unset($params['signature']);
            unset($params['integration_type']);
            unset($params['3ds']);
            $merchantReference = $params['merchant_reference'];
            $calculatedSignature = $this->calculateSignature($params, 'response');
            $success       = true;
            $reason        = '';

            if ($responseSignature != $calculatedSignature) {
                $success = false;
                $reason  = 'Invalid signature.';
                $debugMsg = sprintf('Invalid Signature. Calculated Signature: %1s, Response Signature: %2s', $responseSignature, $calculatedSignature);
                $this->log($debugMsg);
            }
            else {
                $response_code    = $params['response_code'];
                $response_message = $params['response_message'];
                $status           = $params['status'];
                if (substr($response_code, 2) != '000') {
                    $success = false;
                    $reason  = $response_message;
                    $debugMsg = $reason;
                    $this->log($debugMsg);
                }
                else {
                    $success         = true;
                    $host2HostParams = $this->merchantPageNotifyFort($fortParams);
                    $debugMsg = "Fort Merchant Page Host2Hots Response Parameters \n".print_r($fortParams, 1);
                    $this->log($debugMsg);
                    if (!$host2HostParams) {
                        $success = false;
                        $reason  = 'Invalid response parameters.';
                        $debugMsg = $reason;
                        $this->log($debugMsg);
                    }
                    else {
                        $params    = $host2HostParams;
                        $responseSignature = $host2HostParams['signature'];
                        $merchantReference = $params['merchant_reference'];
                        unset($params['r']);
                        unset($params['signature']);
                        unset($params['integration_type']);
                        $calculatedSignature = $this->calculateSignature($params, 'response');
                        if ($responseSignature != $calculatedSignature) {
                            $success = false;
                            $reason  = 'Invalid signature.';
                            $debugMsg = sprintf('Invalid Signature. Calculated Signature: %1s, Response Signature: %2s', $responseSignature, $calculatedSignature);
                            $this->log($debugMsg);
                        }
                        else {
                            $response_code = $params['response_code'];
                            if ($response_code == '20064' && isset($params['3ds_url'])) {
                                $success = true;
                                $debugMsg = 'Redirect to 3DS URL : '.$params['3ds_url'];
                                $this->log($debugMsg);
                                echo "<html><body onLoad=\"javascript: window.top.location.href='" . $params['3ds_url'] . "'\"></body></html>";
                                exit;
                                //header('location:'.$params['3ds_url']);
                            }
                            else {
                                if (substr($response_code, 2) != '000') {
                                    $success = false;
                                    $reason  = $host2HostParams['response_message'];
                                    var_dump();
                                    $debugMsg = $reason;
                                    $this->log($debugMsg);
                                    $return_url = $this->getUrl1('api/v1/mobilePaymentDeclined?id='.$id.'&error='.$params);
                                
                                }
                            }
                        }
                    }
                }
            }
            $servername = "127.0.0.1";
            $username = "isddubai_isd";
            $password = "isddubai_isd";
            $dbname = "isddubai_isd";
            
            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }            
            if(!$success) {
                $p = $params;
                $p['error_msg'] = $reason;
                $res=  http_build_query($p);
                parse_str($res, $output);
                $card_number = $output['card_number'];
                //$amount = $output['amount'];
                $merchant_reference = $output['merchant_reference'];
                $desc = "Payment Failed".$amount."by visa Number ".$card_number."(".$reason.")";
                $sql = "UPDATE bookings SET status='2' WHERE id=".$id;
                $sql2 = "INSERT INTO payment_log (log, book_id) VALUES ('$output', '$id')";  
                $sql3 = "INSERT INTO payment (amount,type, book_id,client_id,ref,status,description,rent) VALUES ('$amount', '0', '$id', '$client_id', '$merchant_reference', '2','$desc','$rent')";  
                $conn->query($sql);
                $conn->query($sql2);
                $conn->query($sql3);
                $conn->close();                 
                $return_url = $this->getUrl1('api/v1/mobilePaymentFail?id='.$id.'&amount='.$amoumt.'&client_id='.$client_id.'&error='.http_build_query($p));
            }
            else{
                $return_url = $this->getUrl1('api/v1/mobilePaymentSuccess?id='.$id);
            }
            echo "<html><body onLoad=\"javascript: window.top.location.href='" . $return_url . "'\"></body></html>";
            echo "<script>window.close();</script>";
            exit;
        }
    }

    public function merchantPageNotifyFort($fortParams)
    {
        session_start();
        $amount = $_SESSION["amount"];
        //send host to host
        if ($this->sandboxMode) {
            $gatewayUrl = $this->gatewaySandboxHost . 'FortAPI/paymentPage';
        }
        else {
            $gatewayUrl = $this->gatewayHost . 'FortAPI/paymentPage';
        }

        $postData      = array(
            'merchant_reference'  => $fortParams['merchant_reference'],
            'access_code'         => $this->accessCode,
            'command'             => $this->command,
            'merchant_identifier' => $this->merchantIdentifier,
            'customer_ip'         => $_SERVER['REMOTE_ADDR'],
            'amount'              => $this->convertFortAmount($amount, $this->currency),
            'currency'            => strtoupper($this->currency),
            'customer_email'      => $this->customerEmail,
            'customer_name'       => $this->customerName,
            'token_name'          => $fortParams['token_name'],
            'language'            => $this->language,
            'return_url'          => $this->getUrl('route.php?r=processResponse'),
            'remember_me'         => $fortParams["remember_me"]        
        );
        
        if(!empty($merchantPageData['paymentMethod']) && $merchantPageData['paymentMethod'] == 'installments_merchantpage'){
                $postData['installments']            = 'YES';
                $postData['plan_code']               = $fortParams['plan_code'];
                $postData['issuer_code']             = $fortParams['issuer_code'];
                $postData['command']                 = 'PURCHASE';
        }
        
        if(isset($fortParams['3ds']) && $fortParams['3ds'] == 'no') {
            $postData['check_3ds'] = 'NO';
        }
        
        //calculate request signature
        $signature             = $this->calculateSignature($postData, 'request');
        $postData['signature'] = $signature;

        $debugMsg = "Fort Host2Host Request Parameters \n".print_r($postData, 1);
        $this->log($debugMsg);
        
        if ($this->sandboxMode) {
            $gatewayUrl = 'https://sbpaymentservices.payfort.com/FortAPI/paymentApi';
        }
        else {
            $gatewayUrl = 'https://paymentservices.payfort.com/FortAPI/paymentApi';
        }
        
        $array_result = $this->callApi($postData, $gatewayUrl);
        
        $debugMsg = "Fort Host2Host Response Parameters \n".print_r($array_result, 1);
        $this->log($debugMsg);
        
        return  $array_result;
    }

    /**
     * Send host to host request to the Fort
     * @param array $postData
     * @param string $gatewayUrl
     * @return mixed
     */
    public function callApi($postData, $gatewayUrl)
    {
        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        $useragent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0";
        curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json;charset=UTF-8',
                //'Accept: application/json, application/*+json',
                //'Connection:keep-alive'
        ));
        curl_setopt($ch, CURLOPT_URL, $gatewayUrl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_ENCODING, "compress, gzip");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // allow redirects		
        //curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return into a variable
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0); // The number of seconds to wait while trying to connect
        //curl_setopt($ch, CURLOPT_TIMEOUT, Yii::app()->params['apiCallTimeout']); // timeout in seconds
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));

        $response = curl_exec($ch);

        //$response_data = array();
        //parse_str($response, $response_data);
        curl_close($ch);

        $array_result = json_decode($response, true);
        
        if (!$response || empty($array_result)) {
            return false;
        }
        return $array_result;
    }
    
    /**
     * calculate fort signature
     * @param array $arrData
     * @param string $signType request or response
     * @return string fort signature
     */
    public function calculateSignature($arrData, $signType = 'request')
    {
        $shaString             = '';
        ksort($arrData);
        foreach ($arrData as $k => $v) {
            $shaString .= "$k=$v";
        }

        if ($signType == 'request') {
            $shaString = $this->SHARequestPhrase . $shaString . $this->SHARequestPhrase;
        }
        else {
            $shaString = $this->SHAResponsePhrase . $shaString . $this->SHAResponsePhrase;
        }
        $signature = hash($this->SHAType, $shaString);

        return $signature;
    }

    /**
     * Convert Amount with dicemal points
     * @param decimal $amount
     * @param string  $currencyCode
     * @return decimal
     */
    public function convertFortAmount($amount, $currencyCode)
    {
        $new_amount = 0;
        $total = $amount;
        $decimalPoints    = $this->getCurrencyDecimalPoints($currencyCode);
        $new_amount = round($total, $decimalPoints) * (pow(10, $decimalPoints));
        return $new_amount;
    }

    public  function castAmountFromFort($amount, $currencyCode)
    {
        $decimalPoints    = $this->getCurrencyDecimalPoints($currencyCode);
        //return $amount / (pow(10, $decimalPoints));
        $new_amount = round($amount, $decimalPoints) / (pow(10, $decimalPoints));
        return $new_amount;
    }
    
    /**
     * 
     * @param string $currency
     * @param integer 
     */
    public function getCurrencyDecimalPoints($currency)
    {
        $decimalPoint  = 2;
        $arrCurrencies = array(
            'JOD' => 3,
            'KWD' => 3,
            'OMR' => 3,
            'TND' => 3,
            'BHD' => 3,
            'LYD' => 3,
            'IQD' => 3,
        );
        if (isset($arrCurrencies[$currency])) {
            $decimalPoint = $arrCurrencies[$currency];
        }
        return $decimalPoint;
    }

    public function getUrl($path)
    {
        $scheme = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') ? 'https://' : 'http://';
        $url = $scheme . $_SERVER['HTTP_HOST'] . $this->projectUrlPath .'/'. $path;
        return $url;
    }

    public function getUrl1($path)
    {
        $scheme = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') ? 'https://' : 'http://';
        $url = $scheme . $_SERVER['HTTP_HOST'] . $this->projectUrlPath_ .''. $path;
        return $url;
    }

    public function generateMerchantReference()
    {
        return rand(0, getrandmax());
    }
    
    /**
     * Log the error on the disk
     */
    public function log($messages) {
        $messages = "========================================================\n\n".$messages."\n\n";
        $file = __DIR__.'/trace.log';
        if (filesize($file) > 907200) {
            $fp = fopen($file, "r+");
            ftruncate($fp, 0);
            fclose($fp);
        }

        $myfile = fopen($file, "a+");
        fwrite($myfile, $messages);
        fclose($myfile);
    }
    
    
    /**
     * 
     * @param type $po payment option
     * @return string payment option name
     */
    function getPaymentOptionName($po) {
        switch($po) {
            case 'creditcard' : return 'Credit Cards';
            case 'cc_merchantpage' : return 'Credit Cards (Merchant Page)';
            case 'installments_merchantpage' : return 'Installments (Merchant Page)';
            case 'installments' : return 'Installments';
            case 'sadad' : return 'SADAD';
            case 'naps' : return 'NAPS';
            default : return '';
        }
    }
}

?>