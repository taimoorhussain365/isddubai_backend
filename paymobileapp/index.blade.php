<?php session_start();
$_SESSION["order_id"] = $_GET['id']; 
$_SESSION["amount"] = $_GET['amount'];
$_SESSION["client_id"] = $_GET['client_id'];
$_SESSION["rent"] = $_GET['rent'];
$amount = $_GET['amount'];

?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

    <title>ISD Payment Gateway</title>

    <!-- Bootstrap core CSS -->
    <link href="/paymobileapp/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="/paymobileapp/assets/form-validation.css" rel="stylesheet">
    <link href="/paymobileapp/fontawesome/css/fontawesome.css" rel="stylesheet">
    <link href="/paymobileapp/fontawesome/css/brands.css" rel="stylesheet">
    <link href="/paymobileapp/fontawesome/css/solid.css" rel="stylesheet">
</head>

<body>
    <?php 
        $bookings = DB::table('bookings')->find($_GET['id']);
        $sport = DB::table('sports')->where('id',$bookings->sports)->value('name');
        $courts = DB::table('courts')->where('id',$bookings->pitch)->value('name');
    ?>
    <div class="container">
      <div class="py-5 text-center">
        <img class="d-block mx-auto mb-4" src="/paymobileapp/assets/logo2.png" alt="" height="80">
        <div class="order col-md12 col-sm-12">
            <h1>Order Details</h1>
            <p class="note">Please review your order details before proceeding with your payment.</p>
            <div class="row line col-md-8 col-sm-12">
                <div class="col-md-6 col-sm-6 back"><?=$sport?></div>
                <div class="col-md-6 col-sm-6"><?=$courts?></div>
            </div>
            <div class="row line col-md-8 col-sm-12">
                <div class="col-md-6 col-sm-6 back"><?=$bookings->from?> - <?=$bookings->to?></div>
                <div class="col-md-6 col-sm-6"><?=date("l, d F", strtotime($bookings->date));?></div>
            </div>
            <div class="row line col-md-8 col-sm-12">
                <div class="col-md-6 col-sm-6 back">ADD-ONS</div>
                <div class="col-md-6 col-sm-6">
                <?php $products = DB::table('order_products')->where('book_id',$_GET['id'])->get();
                     if(count($products) > 0){
                        foreach ($products as $value) {
                           echo $value->title ." <br>";
                        }
                     }else{
                         echo "No ADD-ONS";
                     }   
                     ?></div>
            </div>
            <div class="row price col-md-8 col-sm-12">
                <div class="col-md-6 col-sm-6 back">Total Amount</div>
                <div class="col-md-6 col-sm-6">AED <?=$_GET['amount']?> </div>
            </div>
        </div>
        <div class="order col-md12 col-sm-12">
            <h1>Credit Card Details</h1>
            <form class="needs-validation" novalidate>
                <div class="row col-md-8 col-sm-12 cen">
                    <div class="col-md-12 col-sm-12 mb-3">
                      <label for="firstName">Full Name on The Card</label>
                      <input type="text" class="form-control" id="payfort_fort_mp2_card_holder_name" name="card_holder_name" placeholder="" value="" required>
                      <div class="invalid-feedback">
                        Valid Full Name on The Card is required.
                      </div>
                    </div>
                </div>
                <div class="row col-md-8 col-sm-12 cen">
                    <div class="col-md-12 col-sm-12 mb-3">
                        <label for="firstName">Credit Card Number</label>

                        <div class="input-group">
                            <input type="text" class="form-control" id="payfort_fort_mp2_card_number" placeholder="0000 0000 0000 0000" value="" required>
                            <div class="input-group-addon">
                                <i class="fas fa-credit-card"></i>
                            </div>
                          </div>
                        <div class="invalid-feedback">
                            Valid Credit Card Number is required.
                          </div>
                        </div>
                    </div>
                    <div class="row col-md-8 col-sm-12 cen">
                        <div class="col-md-6 col-sm-12 mb-3">
                          <label for="firstName">Expiry Date</label>
                          <div class="d-flex justify-content-between">
                            <select class="form-control" id="payfort_fort_mp2_expiry_month" required>
                                  <option value="01">01</option>
                                  <option value="02">02 </option>
                                  <option value="03">03</option>
                                  <option value="04">04</option>
                                  <option value="05">05</option>
                                  <option value="06">06</option>
                                  <option value="07">07</option>
                                  <option value="08">08</option>
                                  <option value="09">09</option>
                                  <option value="10">10</option>
                                  <option value="11">11</option>
                                  <option value="12">12</option>
                            </select>
                            <select class="form-control" name="expiry_year" id="payfort_fort_mp2_expiry_year">
                              <option value="21"> 2021</option>
                              <option value="22"> 2022</option>
                              <option value="23"> 2023</option>
                              <option value="24"> 2024</option>
                              <option value="25"> 2025</option>
                              <option value="26"> 2026</option>
                              <option value="27"> 2027</option>
                            </select> 
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-12 mb-3">
                          <label for="firstName">CVV</label>
                          <input type="text" class="form-control" id="payfort_fort_mp2_cvv" placeholder="" value="" required>
                          <div class="invalid-feedback">
                            Valid Full Name on The Card is required.
                          </div>
                        </div>
                        <div class="col-12">
                            <input type="checkbox" name="remember_me" id="payfort_fort_mp2_remember_me" maxlength="4">
                            <label style="float: left;margin-right: 10px;">Save Card</label>
                        </div>
                    </div>
            </form>
        </div>
        <div class="row">
        <div class="col-md-4 col-sm-12  align-self-end">
            <button class="btn btn-primary btn-lg btn-block" id="btn_continue" type="submit">Pay Now</button>
        </div>
    </div>
    </form>
       
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="bootstrap/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="/paymobileapp/bootstrap/assets/js/vendor/popper.min.js"></script>
    <script src="/paymobileapp/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/paymobileapp/bootstrap/assets/js/vendor/holder.min.js"></script>
    <script>
      // Example starter JavaScript for disabling form submissions if there are invalid fields
      (function() {
        'use strict';

        window.addEventListener('load', function() {
          // Fetch all the forms we want to apply custom Bootstrap validation styles to
          var forms = document.getElementsByClassName('needs-validation');

          // Loop over them and prevent submission
          var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
              if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
              }
              form.classList.add('was-validated');
            }, false);
          });
        }, false);
      })();
    </script>
    


        <script type="text/javascript" src="/paymobileapp/vendors/jquery.min.js"></script>
        <script type="text/javascript" src="/paymobileapp/assets/js/jquery.creditCardValidator.js"></script>
        <script type="text/javascript" src="/paymobileapp/assets/js/checkout.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {


                //getPaymentPage("cc_merchantpage");
                var remember_me = "NO";
                $('input[type="checkbox"]').click(function () {
                    if ($(this).prop("checked") == true) {
                        remember_me = "YES";
                        console.log("YES");

                    } else if ($(this).prop("checked") == false) {
                        remember_me = "NO";
                        console.log("NO");
                    }
                });


                $('input:radio[name=payment_option]').click(function () {
                    $('input:radio[name=payment_option]').each(function () {
                        if ($(this).is(':checked')) {
                            $(this).addClass('active');
                            $(this).parent('li').children('label').css('font-weight', 'bold');
                            $(this).parent('li').children('div.details').show();
                        } else {
                            $(this).removeClass('active');
                            $(this).parent('li').children('label').css('font-weight', 'normal');
                            $(this).parent('li').children('div.details').hide();
                        }
                    });
                });
                $('#btn_continue').click(function () {
                    var paymentMethod = "cc_merchantpage2";
                    if (paymentMethod == 'cc_merchantpage2') {
                        var isValid = payfortFortMerchantPage2.validateCcForm();
                        if (isValid) {
                            getPaymentPage(paymentMethod, remember_me);
                        }
                    } else {
                        getPaymentPage(paymentMethod);
                    }
                });
            });
        </script>
    </div>
    <footer>
        <script>
            document.body.addEventListener("wheel", e => {
                if (e.ctrlKey)
                    e.preventDefault(); //prevent zoom
            });
        </script>
    </footer>
</body>

</html>